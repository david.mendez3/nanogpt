{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "x_p8n4g8FYng"
      },
      "source": [
        "# Intro to Graph Neural Networks\n",
        "\n",
        "Just a simple PyTorch implementation of the tutorial in https://www.youtube.com/watch?v=8owQBFAHw7E"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 1,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "_etjO7pJs2Z8",
        "outputId": "a22fb942-9640-4e71-9a74-87f002307ca2"
      },
      "outputs": [
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "2023-08-10 16:56:45.002621: I tensorflow/core/platform/cpu_feature_guard.cc:182] This TensorFlow binary is optimized to use available CPU instructions in performance-critical operations.\n",
            "To enable the following instructions: AVX2 FMA, in other operations, rebuild TensorFlow with the appropriate compiler flags.\n",
            "2023-08-10 16:56:45.688666: W tensorflow/compiler/tf2tensorrt/utils/py_utils.cc:38] TF-TRT Warning: Could not find TensorRT\n"
          ]
        }
      ],
      "source": [
        "import numpy as np\n",
        "import torch\n",
        "import spektral # for preprocessing the dataset\n",
        "from tqdm import tqdm"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 2,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "ukVlrvo4tOlJ",
        "outputId": "40becf9f-7463-464b-ed81-6a682ddaf634"
      },
      "outputs": [
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "/home/deivi/miniconda3/envs/amlengine/lib/python3.10/site-packages/scipy/sparse/_index.py:146: SparseEfficiencyWarning: Changing the sparsity structure of a csr_matrix is expensive. lil_matrix is more efficient.\n",
            "  self._set_arrayXarray(i, j, x)\n"
          ]
        }
      ],
      "source": [
        "dataset = spektral.datasets.Citation(\"cora\")\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 3,
      "metadata": {
        "id": "Wr5o9pq4wamA"
      },
      "outputs": [],
      "source": [
        "mask_train, mask_validation, mask_test = dataset.mask_tr, dataset.mask_va, dataset.mask_te"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 4,
      "metadata": {
        "id": "ju5xQjOjvZ7q"
      },
      "outputs": [],
      "source": [
        "adjacency = torch.tensor(dataset.graphs[0].a.todense()).float()\n",
        "adjacency += torch.eye(adjacency.shape[0])\n",
        "features = torch.tensor(dataset.graphs[0].x).float()\n",
        "labels = torch.tensor(dataset.graphs[0].y).float()"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "SmKkexrRvulx"
      },
      "source": [
        "The dataset is small enough that we can process is as a dense matrix. We also add self-connections."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "jA3rZopCwRuK"
      },
      "source": [
        "Some data about the dataset."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 5,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "u7xjNoiywJmH",
        "outputId": "f57ae6a9-2958-40fa-f3c4-429b7bc39df9"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "Features shape (features per node): torch.Size([2708, 1433])\n",
            "Adjacency shape (number nodes): torch.Size([2708, 2708])\n",
            "Labels (num topics): torch.Size([2708, 7])\n",
            "Num train: 140\n",
            "Num val: 500\n",
            "Num test: 1000\n"
          ]
        }
      ],
      "source": [
        "print(f\"Features shape (features per node): {features.shape}\")\n",
        "print(f\"Adjacency shape (number nodes): {adjacency.shape}\")\n",
        "print(f\"Labels (num topics): {labels.shape}\")\n",
        "print(f\"Num train: {np.sum(mask_train)}\")\n",
        "print(f\"Num val: {np.sum(mask_validation)}\")\n",
        "print(f\"Num test: {np.sum(mask_test)}\")\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The Cora dataset consists on 2708 scientific publications classified into seven classes (labels shape). The graph represents the citation network, namely which papers cited which. The features represent the presence of 1433 different words in each of the publications. Namely, for one of the 1433 words, there is a 1 on the corresponding entry if the word is present in a paper and a 0 otherwise.\n",
        "\n",
        "The task we are going to use is trying to predict the class of the publication based on the citation network and the words present. We will see how using the connections lets us increase the accuracy versus just utilizing the feature vectors for each of the papers.\n",
        "\n",
        "More information on the dataset at https://relational.fit.cvut.cz/dataset/CORA"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 6,
      "metadata": {
        "id": "z261tkHMw91b"
      },
      "outputs": [],
      "source": [
        "def masked_softmax_cross_entropy(logits, labels, mask):\n",
        "  loss = torch.nn.functional.cross_entropy(logits, labels, reduction=\"none\")\n",
        "  return torch.mean(loss[mask])\n",
        "\n",
        "def masked_accuracy(logits, labels, mask):\n",
        "  correct_prediction = torch.argmax(labels, 1) == torch.argmax(logits, 1)\n",
        "  correct_prediction = correct_prediction[mask]\n",
        "  return torch.mean(correct_prediction.float())\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "UsXzgv9m3nRe"
      },
      "source": [
        "Let us define a simple graph layer, where the matrix is represented as a `torch.nn.Linear` layer. We thus inherit from that class."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 7,
      "metadata": {
        "id": "cuShYWGbyQsx"
      },
      "outputs": [],
      "source": [
        "class GNN(torch.nn.Linear):\n",
        "  \"\"\"\n",
        "  Remember the update: sigma(AHW). Transform will be the equivalent of our matrix W\n",
        "  (likey a linear layer), features will be our previous layer info for update.\n",
        "  \"\"\"\n",
        "  def __init__(self, adjacencies, in_features, out_features, activation):\n",
        "    super().__init__(in_features = in_features, out_features = out_features)\n",
        "\n",
        "    self.adjacencies = adjacencies\n",
        "    self.activation = activation\n",
        "\n",
        "  def forward(self, features: torch.Tensor):\n",
        "    x = super(GNN, self).forward(features)\n",
        "    x = self.adjacencies @ x\n",
        "    return self.activation(x)"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 8,
      "metadata": {
        "id": "oWZZVyKNzhrH"
      },
      "outputs": [],
      "source": [
        "class CoraNetwork(torch.nn.Module):\n",
        "  def __init__(self, adjacencies, in_features, hidden, out_features, activation):\n",
        "    super().__init__()\n",
        "\n",
        "    self.gnn_layer_1 = GNN(adjacencies, in_features, hidden, activation)\n",
        "    self.gnn_layer_2 = GNN(adjacencies, hidden, out_features, torch.nn.Identity())\n",
        "\n",
        "  def forward(self, features):\n",
        "    x = self.gnn_layer_1(features)\n",
        "    x = self.gnn_layer_2(x)\n",
        "    return x"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 9,
      "metadata": {
        "id": "Nne7oEil4esJ"
      },
      "outputs": [],
      "source": [
        "def train(network, features, labels, epochs, lr, mask_train, mask_val, patience=0):\n",
        "  optimizer = torch.optim.Adam(network.parameters(), lr=lr)\n",
        "\n",
        "  losses, val_acc = [], []\n",
        "  best_val_acc = 0\n",
        "  times_worse = 0\n",
        "\n",
        "  with tqdm(range(epochs), unit=\"epoch\") as tepochs:\n",
        "    tepochs.set_description(\"Training\")\n",
        "    for epoch in tepochs:\n",
        "      network.train()\n",
        "      optimizer.zero_grad()\n",
        "      logits = network(features)\n",
        "      loss = masked_softmax_cross_entropy(logits, labels, mask_train)\n",
        "      loss.backward()\n",
        "      optimizer.step()\n",
        "\n",
        "      losses.append(loss.item())\n",
        "\n",
        "      network.eval()\n",
        "      logits = network(features)\n",
        "      val_accuracy = masked_accuracy(logits, labels, mask_val).item()\n",
        "      val_acc.append(val_accuracy)\n",
        "\n",
        "      tepochs.set_postfix(\n",
        "          loss=\"{:.5f}\".format(losses[-1]), val_acc=val_acc[-1]\n",
        "      )\n",
        "\n",
        "      if val_accuracy < best_val_acc:\n",
        "        times_worse += 1\n",
        "        if patience > 0 and times_worse >= patience:\n",
        "          print(f\"\\nEarly stop. The best validation accuracy was {best_val_acc * 100:.3f}%\")\n",
        "          return losses, val_acc\n",
        "      else:\n",
        "        best_val_acc = val_accuracy\n",
        "        times_worse = 0\n",
        "\n",
        "    print(f\"\\nThe best validation accuracy was {best_val_acc * 100:.3f}%\")\n",
        "    return losses, val_acc\n",
        "\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 10,
      "metadata": {
        "id": "8e_6anWN7fMP"
      },
      "outputs": [],
      "source": [
        "cora_network = CoraNetwork(\n",
        "    adjacencies = adjacency,\n",
        "    in_features = features.shape[1],\n",
        "    out_features = labels.shape[1],\n",
        "    hidden = 32,\n",
        "    activation = torch.nn.ReLU()\n",
        ")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 11,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "f98eQrnV7vRq",
        "outputId": "9b303703-c6e7-497e-acb5-63761ce06d7d"
      },
      "outputs": [
        {
          "data": {
            "text/plain": [
              "CoraNetwork(\n",
              "  (gnn_layer_1): GNN(\n",
              "    in_features=1433, out_features=32, bias=True\n",
              "    (activation): ReLU()\n",
              "  )\n",
              "  (gnn_layer_2): GNN(\n",
              "    in_features=32, out_features=7, bias=True\n",
              "    (activation): Identity()\n",
              "  )\n",
              ")"
            ]
          },
          "execution_count": 11,
          "metadata": {},
          "output_type": "execute_result"
        }
      ],
      "source": [
        "cora_network"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 12,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "5Pte2DTo7wCd",
        "outputId": "722d1db1-8858-4cc8-8eb9-a1ab8f34d9d7"
      },
      "outputs": [
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "Training: 100%|██████████| 200/200 [00:04<00:00, 49.01epoch/s, loss=0.00017, val_acc=0.732]"
          ]
        },
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "\n",
            "The best validation accuracy was 76.600%\n"
          ]
        },
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "\n"
          ]
        }
      ],
      "source": [
        "_, _ = train(cora_network, features, labels, 200, 0.01, mask_train, mask_validation)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Y4oa-MfE_bRX"
      },
      "source": [
        "What if we don't use a graph? We can get those results by using the identity matrix as adjacency, which would be equivalent to just using an MLP classifier on the words present on each of the papers."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 13,
      "metadata": {
        "id": "8Eh_q8039726"
      },
      "outputs": [],
      "source": [
        "cora_linear_network = CoraNetwork(\n",
        "    adjacencies = torch.eye(features.shape[0]),\n",
        "    in_features = features.shape[1],\n",
        "    out_features = labels.shape[1],\n",
        "    hidden = 32,\n",
        "    activation = torch.nn.ReLU()\n",
        ")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 14,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "O2xeNS-m_qON",
        "outputId": "a3c4d944-f035-4ce3-caa2-6ff56b14e517"
      },
      "outputs": [
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "Training: 100%|██████████| 200/200 [00:04<00:00, 46.59epoch/s, loss=0.00029, val_acc=0.508]"
          ]
        },
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "\n",
            "The best validation accuracy was 55.400%\n"
          ]
        },
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "\n"
          ]
        }
      ],
      "source": [
        "_, _ = train(cora_linear_network, features, labels, 200, 0.01, mask_train, mask_validation)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "1rP6tQ_a_wK7"
      },
      "source": [
        "We can see how our accuracy plateaus earlier!"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Y-RMN8L4Ac67"
      },
      "source": [
        "We passed the adjacency matrix as is to the network, which means we are performing sum pooling. This increases the scale of the output features! We can instead normalize by the degree of the vertices, so this does not happen.\n",
        "\n",
        "Let us first do mean pooling."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 15,
      "metadata": {
        "id": "j1n9vSti_sK4"
      },
      "outputs": [],
      "source": [
        "degree_matrix = torch.sum(adjacency, dim=-1)"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 16,
      "metadata": {
        "id": "5VmQLsNbBTEQ"
      },
      "outputs": [],
      "source": [
        "cora_network_meanpooling = CoraNetwork(\n",
        "    adjacencies = adjacency / degree_matrix,\n",
        "    in_features = features.shape[1],\n",
        "    out_features = labels.shape[1],\n",
        "    hidden = 32,\n",
        "    activation = torch.nn.ReLU()\n",
        ")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 17,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "54kEuPyjBUdL",
        "outputId": "7031c43f-1d8d-475f-c205-8c7e15174354"
      },
      "outputs": [
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "Training: 100%|██████████| 200/200 [00:04<00:00, 45.27epoch/s, loss=0.00037, val_acc=0.758]"
          ]
        },
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "\n",
            "The best validation accuracy was 77.000%\n"
          ]
        },
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "\n"
          ]
        }
      ],
      "source": [
        "_, _ = train(cora_network_meanpooling, features, labels, 200, 0.01, mask_train, mask_validation)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "QFvRKSHJByIK"
      },
      "source": [
        "We see slightly better and more stable results!"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Y9jpR1J_CR3D"
      },
      "source": [
        "We now try the GCN (graph convolutional network) normalization. This requires computing 1 over the square root of the degrees and multiplying on both sides."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 18,
      "metadata": {
        "id": "kqWRYGm7CFHq"
      },
      "outputs": [],
      "source": [
        "norm_deg = torch.diag(1.0 / torch.sqrt(degree_matrix))\n",
        "norm_adj = norm_deg @ adjacency @ norm_deg"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 19,
      "metadata": {
        "id": "vHpWWo3MCmfP"
      },
      "outputs": [],
      "source": [
        "cora_network_gcn = CoraNetwork(\n",
        "    adjacencies = norm_adj,\n",
        "    in_features = features.shape[1],\n",
        "    out_features = labels.shape[1],\n",
        "    hidden = 32,\n",
        "    activation = torch.nn.ReLU()\n",
        ")"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 20,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "8DjKRHw1Cv7p",
        "outputId": "66254533-0c03-427f-fe1d-baed6a9c94a4"
      },
      "outputs": [
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "Training:   0%|          | 0/200 [00:00<?, ?epoch/s, loss=1.85473, val_acc=0.42] "
          ]
        },
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "Training: 100%|██████████| 200/200 [00:04<00:00, 45.98epoch/s, loss=0.00028, val_acc=0.78] "
          ]
        },
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "\n",
            "The best validation accuracy was 79.600%\n"
          ]
        },
        {
          "name": "stderr",
          "output_type": "stream",
          "text": [
            "\n"
          ]
        }
      ],
      "source": [
        "_, _ = train(cora_network_gcn, features, labels, 200, 0.01, mask_train, mask_validation)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "What is our final test accuracy?"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 21,
      "metadata": {},
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "Test accuracy: 78.700%.\n"
          ]
        }
      ],
      "source": [
        "print(f\"Test accuracy: {masked_accuracy(cora_network_gcn(features), labels, mask_test) * 100:.3f}%.\")"
      ]
    }
  ],
  "metadata": {
    "colab": {
      "provenance": []
    },
    "kernelspec": {
      "display_name": "Python 3",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.10.10"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}
