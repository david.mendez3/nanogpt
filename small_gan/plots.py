import matplotlib.pyplot as plt
import torch.nn as nn
import torch

def plot_sin_dataset(data):
    plt.plot(data[:,0], data[:,1], ".")
    plt.show()

def plot_losses(loss_gen: list, loss_disc: list):
    epochs = len(loss_gen)
    fig, ax = plt.subplots()
    x_axis = list(range(epochs))
    ax.plot(x_axis, loss_gen, label="Generator loss", color="red")
    ax.plot(x_axis, loss_disc, label="Discriminator loss", color="orange")
    ax.set_xlabel("Epochs")
    ax.set_ylabel("Loss")
    ax.legend()

def generate_and_plot_sin(generator: nn.Module, number_points: int):
    latent_space_samples = torch.randn((number_points, 2))
    with torch.no_grad():
        generated_samples = generator(latent_space_samples)
    plot_sin_dataset(generated_samples)

def plot_mnist_image(image: torch.Tensor):
    plt.imshow(image.reshape(28,28).cpu().detach().numpy(), cmap="gray_r")
    plt.xticks([])
    plt.yticks([])
    plt.show()

def plot_16_mnist_images(images: torch.Tensor):
    for i in range(16):
        ax = plt.subplot(4, 4, i + 1)
        plt.imshow(images[i].reshape(28, 28).cpu(), cmap="gray_r")
        plt.xticks([])
        plt.yticks([])

def generate_and_plot_mnist(generator: nn.Module, device: str):
    latent_space_samples = torch.randn((16, 100), device=device)
    with torch.no_grad():
        images = generator(latent_space_samples)
    plot_16_mnist_images(images)