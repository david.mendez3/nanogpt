import torch
import torch.nn as nn

from tqdm import tqdm


def train_loop(
    generator: nn.Module,
    discriminator: nn.Module,
    train_loader: torch.utils.data.DataLoader,
    epochs: int,
    lr: float,
    device: str = "cpu",
):
    generator_optimizer = torch.optim.Adam(params=generator.parameters(), lr=lr)
    discriminator_optimizer = torch.optim.Adam(params=discriminator.parameters(), lr=lr)
    loss_fn = torch.nn.BCELoss()  # Binary cross entropy

    latent_size = generator.model[0].in_features

    with tqdm(range(epochs), unit="epoch") as tepochs:
        losses_generator = []
        losses_discriminator = []

        tepochs.set_description("Training")
        for epoch in tepochs:
            epoch_gen_loss = 0
            epoch_disc_loss = 0
            cum_num_samples = 0
            for real_samples, _ in train_loader:
                # Data for training the discriminator
                real_samples = real_samples.to(device)
                num_samples = real_samples.shape[0]
                cum_num_samples += num_samples
                real_samples_labels = torch.ones((num_samples, 1)).to(device)
                latent_space_samples = torch.randn((num_samples, latent_size)).to(
                    device
                )
                generated_samples = generator(latent_space_samples)
                generated_samples_labels = torch.zeros((num_samples, 1)).to(device)
                all_samples = torch.cat((real_samples, generated_samples))
                all_samples_labels = torch.cat(
                    (real_samples_labels, generated_samples_labels)
                )

                # Training the discriminator
                discriminator.zero_grad()
                output_discriminator = discriminator(all_samples)
                loss_discriminator = loss_fn(output_discriminator, all_samples_labels)
                loss_discriminator.backward()
                discriminator_optimizer.step()

                # Save losses discriminator
                epoch_disc_loss += loss_discriminator.item()

                # Data for the generator
                latent_space_samples = torch.randn((num_samples, latent_size)).to(
                    device
                )

                # Training the generator
                generator.zero_grad()
                generated_samples = generator(latent_space_samples)
                output_discriminator_generated = discriminator(generated_samples)
                loss_generator = loss_fn(
                    output_discriminator_generated, real_samples_labels
                )
                loss_generator.backward()
                generator_optimizer.step()

                # Save losses generator
                epoch_gen_loss += loss_generator.item()

            losses_discriminator.append(epoch_disc_loss / cum_num_samples)
            losses_generator.append(epoch_gen_loss / cum_num_samples)

            # Set tqdm postfix
            tepochs.set_postfix(
                gen_loss="{:.5f}".format(losses_generator[-1]),
                disc_loss="{:.5f}".format(losses_discriminator[-1]),
            )


    return losses_generator, losses_discriminator
