# We generate points from the curve of a sine as train data
# Since these are the real examples, they're all labelled zero

import torch

import torchvision
import torchvision.transforms as transforms

from math import pi

def sin_data(num_points: int) -> tuple[torch.Tensor, torch.Tensor]:
    train_data = torch.zeros((num_points, 2))
    train_data[:, 0] = 2 * pi * torch.rand(num_points)
    train_data[:, 1] = torch.sin(train_data[:, 0])
    train_labels = torch.ones(num_points)
    return train_data, train_labels

def sin_dataloader(num_points: int, batch_size: int = 32) -> torch.utils.data.dataloader.DataLoader:
    train_data, train_labels = sin_data(num_points)
    return torch.utils.data.DataLoader(
        torch.utils.data.TensorDataset(train_data, train_labels), batch_size=batch_size, shuffle=True 
    )

def mnist_dataloader(batch_size):
    transform = transforms.Compose(
        [transforms.ToTensor(), transforms.Normalize((0.5,), (0.5,))]
    )

    train_set = torchvision.datasets.MNIST(
        root=".", train=True, download=True, transform=transform
    )

    train_loader = torch.utils.data.DataLoader(
        train_set, batch_size=batch_size, shuffle=True
    )

    return train_loader